package Utilities;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;


public class CaliforniaSharedPreferenceUtil {

	private final static String TAG = CaliforniaSharedPreferenceUtil.class.getName();
    private static SharedPreferences _pref;
	
	private static CaliforniaSharedPreferenceUtil _instance;
	
	public static final String SHARED_PREF_NAME = "california_sp";
	public static final int PRIVATE_MODE = 0;
	
	private enum Keys {
		
		IS_RECOEDER_START("is_recorder_start");
				
		private String label;
		
		private Keys(String label) {
			this.label = label;
		}

		public String getLabel() {
			return label;
		}
	}
	
	private CaliforniaSharedPreferenceUtil() {}
	
	public static CaliforniaSharedPreferenceUtil getInstance(Context context) {
		if(_pref == null) {
			_pref = context.getSharedPreferences(SHARED_PREF_NAME, PRIVATE_MODE);
		}
		if(_instance == null ) {
			_instance = new CaliforniaSharedPreferenceUtil();
		}
		return _instance;
	}
	
	
	public boolean isRecorderStart(boolean defaultValue){
		return getBoolean(Keys.IS_RECOEDER_START.getLabel(), defaultValue);
	}
	
	public void setUserID(boolean value){
		setBoolean(Keys.IS_RECOEDER_START.getLabel(), value);
	}
	
	

	
	private void setString(String key, String value) {
		if(key != null && value != null ) {
			try {
				if(_pref != null) {
					Editor editor = _pref.edit();
					editor.putString(key, value);
					editor.commit();
				}
			} catch (Exception e) {
				Log.e(TAG, "Unable to set " + key + "= " + value + "in shared preference", e);
			}
		}
	}
	
	private void setLong(String key, long value) {
		if(key != null) {
			try {
				if(_pref != null) {
					Editor editor = _pref.edit();
					editor.putLong(key, value);
					editor.commit();
				}
			} catch (Exception e) {
				Log.e(TAG, "Unable to set " + key + "= " + value + "in shared preference", e);
			}
		}
	}
	
	private void setInt(String key, int value) {
		if(key != null) {
			try {
				if(_pref != null) {
					Editor editor = _pref.edit();
					editor.putInt(key, value);
					editor.commit();
				}
			} catch (Exception e) {
				Log.e(TAG, "Unable to set " + key + "= " + value + "in shared preference", e);
			}
		}
	}
	
	private void setDouble(String key, double value) {
		if(key != null) {
			try {
				if(_pref != null) {
					Editor editor = _pref.edit();
					editor.putFloat(key, (float) value);
					editor.commit();
				}
			} catch (Exception e) {
				Log.e(TAG, "Unable to set " + key + "= " + value + "in shared preference", e);
			}
		}
	}
	
	private void setBoolean(String key, boolean value) {
		if(key != null) {
			try {
				if(_pref != null) {
					Editor editor = _pref.edit();
					editor.putBoolean(key, value);
					editor.commit();
				}
			} catch (Exception e) {
				Log.e(TAG, "Unable to set " + key + "= " + value + "in shared preference", e);
			}
		}
	}
	
	private int getInt(String key, int defaultValue) {
		if(_pref != null && key != null && _pref.contains(key)) {
			return _pref.getInt(key, defaultValue);
		}
		return defaultValue;
	}
	
	private long getLong(String key, long defaultValue) {
		if(_pref != null && key != null && _pref.contains(key)) {
			return _pref.getLong(key, defaultValue);
		}
		return defaultValue;
	}
	
	private boolean getBoolean(String key, boolean defaultValue) {
		if(_pref != null && key != null && _pref.contains(key)) {
			return _pref.getBoolean(key, defaultValue);
		}
		return defaultValue;
	}
	
	private String getString(String key, String defaultValue) {
		if(_pref != null && key != null && _pref.contains(key)) {
			return _pref.getString(key, defaultValue);
		}
		return defaultValue;
	}
	private double getDouble(String key, double defaultValue) {
		if(_pref != null && key != null && _pref.contains(key)) {
			return _pref.getFloat(key, (float) defaultValue);
		}
		return defaultValue;
	}
	
	private void removeString(String key) {
		if(key != null) {
			try {
				if(_pref != null && _pref.contains(key)) {
					Editor editor = _pref.edit();
					editor.remove(key);
					editor.commit();
				}
			} catch (Exception e) {
				Log.e(TAG, "Unable to remove key" + key, e);
			}
		}
	}
	
	/**
	 * This Method Clear shared preference.
	 */
	public void clear() {
		Editor editor = _pref.edit();
		editor.clear();
		editor.commit();
	}
	
}
