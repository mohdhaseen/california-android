package Utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import android.os.Environment;

public class UtilToCopyDB {
	 public static void exportDB(){
		 File sampleDir = new File(Environment.getExternalStorageDirectory(),
					"/fbappDB");
			if (!sampleDir.exists()) {
				sampleDir.mkdirs();
			}
	        File sd = Environment.getExternalStorageDirectory();
	           File data = Environment.getDataDirectory();
	           FileChannel source=null;
	           FileChannel destination=null;
	           String currentDBPath = "/data/"+ "com.facebook.orca" +"/databases/"+"threads_db2";
	           
	           String backupDBPath = "/fbappDB/threads_db2_copy";
	           File currentDB = new File(data, currentDBPath);
	           File backupDB = new File(sd, backupDBPath);
	           try {
	                source = new FileInputStream(currentDB).getChannel();
	                destination = new FileOutputStream(backupDB).getChannel();
	                destination.transferFrom(source, 0, source.size());
	                source.close();
	                destination.close();
	                
	            } catch(IOException e) {
	                e.printStackTrace();
	            }
	    }
	 


}
