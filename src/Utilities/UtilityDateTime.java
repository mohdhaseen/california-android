package Utilities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Locale;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

public class UtilityDateTime {
	
	public static String getDateFromTimestamp(long timestamp) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Date date = new Date(timestamp);
        return sdf.format(date);
    }
 
	public static File copyImageFile(Context context, Uri imageUri) {
        try {
             File rootSdDirectory = Environment.getExternalStorageDirectory();
             File casted_image = new File(rootSdDirectory, "attachment.jpg");
            if (casted_image.exists()) {
                casted_image.delete();
            }
                casted_image.createNewFile();

                InputStream inputStream = context.getContentResolver().openInputStream(imageUri);
                        FileOutputStream fileOutputStream = new FileOutputStream(casted_image);
                        copyStream(inputStream, fileOutputStream);
                        fileOutputStream.close();
                        inputStream.close();
                return casted_image;


        } catch (Exception e) {

            System.out.print(e);
            // e.printStackTrace();

        }
        return null;
        }
    
    
    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
            }
            }
 



}
