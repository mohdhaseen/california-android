package com.craterzone.california.models;

public class FBModel {
	private String msgId;
	public String getMsgId() {
		return msgId;
	}
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getAttachment() {
		return attachment;
	}
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}
	public String getThreadKey() {
		return threadKey;
	}
	public void setThreadKey(String threadKey) {
		this.threadKey = threadKey;
	}
	public String getParticipants() {
		return participants;
	}
	public void setParticipants(String participants) {
		this.participants = participants;
	}
	
	private String text;
	private String sender;
	private String attachment;
	private String threadKey;
	private String participants;
	private String coordinates;
	private String pendingMedia;
	public String getPendingMedia() {
		return pendingMedia;
	}
	public void setPendingMedia(String pendingMedia) {
		this.pendingMedia = pendingMedia;
	}
	public String getCoordinates() {
		return coordinates;
	}
	public void setCoordinates(String coordinates) {
		this.coordinates = coordinates;
	}

}
