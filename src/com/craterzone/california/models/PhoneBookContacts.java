package com.craterzone.california.models;

import java.util.ArrayList;

import android.graphics.Bitmap;

public class PhoneBookContacts {
	public String name;
	public ArrayList<String> emails=new ArrayList<String>();
	public ArrayList<String> phoneNumbers = new ArrayList<String>();
	public Bitmap bitmap;
	public Long contactId;

	public Long getContactId() {
		return contactId;
	}

	public String getName() {
		return name;
	}

	public ArrayList<String> getEmails() {
		return emails;
	}

	public ArrayList<String> getPhoneNumbers() {
		return phoneNumbers;
	}

	public Bitmap getPhoto() {
		return bitmap;
	}

	public void addPhoneNumber(String number) {
		if (number == null || number.trim().equals("")) {
			return;
		}
		phoneNumbers.add(number);
	}

	public void addEmail(String email) {
		if (email == null || email.trim().equals("")) {
			return;
		}
		emails.add(email);
	}
}
