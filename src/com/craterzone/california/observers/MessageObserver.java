package com.craterzone.california.observers;

import java.sql.Date;

import android.R.bool;
import android.R.string;
import android.content.Context;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by mohdhaseen on 7/5/15.
 */
public class MessageObserver extends ContentObserver {
	private static final int MESSAGE_TYPE_SENT = 2;
	private Context c;
	private static Boolean isSent = true;

	public MessageObserver(Handler handler, Context context) {
		super(handler);
		c = context;
	}

	@Override
	public void onChange(boolean selfChange) {
		onChange(selfChange, null);

	}

	@Override
	public boolean deliverSelfNotifications() {
		
		return super.deliverSelfNotifications();
	}

	@Override
	public void onChange(boolean selfChange, Uri uri) {
		Log.i("TAG_TO_TESTSelfChangeSMS",""+selfChange );
		if (uri != null) {

			try {
				Cursor cursor = c.getContentResolver().query((uri), null, null,
						null, null);
				if (cursor.moveToNext()) {
					// String protocol =
					// cursor.getString(cursor.getColumnIndex("protocol"));
					/* String contentType =
					 cursor.getString(cursor.getColumnIndex("ct_t"));*/
					int type = cursor.getInt(cursor.getColumnIndex("type"));

					if (type != MESSAGE_TYPE_SENT) {

						return;
					}
					/*if(isSent)
					{*/
					int dateColumn = cursor.getColumnIndex("date");
					int bodyColumn = cursor.getColumnIndex("body");
					int addressColumn = cursor.getColumnIndex("address");

					String to = cursor.getString(addressColumn);
					Date now = new Date(cursor.getLong(dateColumn));
					String message = cursor.getString(bodyColumn);
					
					Toast.makeText(c, "Message Sent", Toast.LENGTH_SHORT).show();
					Log.i("TAG_TO_TESTTO", to);
					Log.i("TAG_TO_TESTDATE", now.toString());
					Log.i("TAG_TO_TESTBODY", message);
				
					Toast.makeText(c, "Date "+now, Toast.LENGTH_SHORT).show();
					Toast.makeText(c, "To "+to, Toast.LENGTH_SHORT).show();
					Toast.makeText(c, "Message Body "+message, Toast.LENGTH_SHORT).show();
					/*isSent=false;
					}
					else
					{
						isSent=true;
					}*/
				} 
cursor.close();
			} catch (IllegalStateException e) {
				Log.i("ExceptionOccurs",e.toString());
				e.printStackTrace();
			}	
			catch (Exception e) {
				Log.i("ExceptionOccurs",e.toString());
				e.printStackTrace();
			}}
		else {
			 Toast.makeText(c,"null uri please ignore this ",
			 Toast.LENGTH_SHORT).show();
		}
	}
}
