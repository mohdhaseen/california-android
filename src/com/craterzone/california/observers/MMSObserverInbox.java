package com.craterzone.california.observers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.Calendar;
import Utilities.UtilityDateTime;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;

public class MMSObserverInbox extends ContentObserver {
	private Context c;
	public static final int MESSAGE_BOX_INBOX =1;

	public MMSObserverInbox(Handler handler, Context context) {
		super(handler);
		c = context;
	}

	@Override
	public boolean deliverSelfNotifications() {
		// TODO Auto-generated method stub
		return super.deliverSelfNotifications();
	}

	@Override
	public void onChange(boolean selfChange, Uri uri) {
		Log.d("TAG_TO_TESTSelfChangeMMSInbox", "" + selfChange);

		if (uri != null) {

			Cursor mCursor = c.getContentResolver().query((uri), null, null,
					null, null);
			if (mCursor.moveToNext()) {
				// String type =
				// mCursor.getString(mCursor.getColumnIndex("ct_t"));
				String mms_id = mCursor
						.getString(mCursor.getColumnIndex("_id"));
				int type = mCursor.getInt(mCursor.getColumnIndex("msg_box"));

				if (type != MESSAGE_BOX_INBOX){
			/*	if(type !=MESSAGE_BOX_INBOX)*/
					return;
				}
				Log.d("TAG_TO_TESTid=", mms_id);
				Log.d("TAG_TO_TESTtype 1-in,4-out=", "" + type);
				String subject = mCursor.getString(mCursor
						.getColumnIndex("sub"));
				Calendar calendar = Calendar.getInstance();
				String date = UtilityDateTime.getDateFromTimestamp(calendar
						.getTimeInMillis());
				Log.d("TAG_TO_TESTSubject:", "" + subject);
				//Toast.makeText(c, ""+subject, Toast.LENGTH_SHORT).show();
				Log.d("TAG_TO_TESTDate", "" + date);
				//Toast.makeText(c, ""+date, Toast.LENGTH_SHORT).show();
				// getting text part of mms
				String selectionPart = "mid=" + mms_id;
				Uri muri = Uri.parse("content://mms/part");
				getTextFromMMS(selectionPart, muri);
				getImageFromMMS(selectionPart, muri);
String mmsAddress=getMMSAddress(c, mms_id);
Log.d("TAG_TO_TESTAddress", "" + mmsAddress);
//Toast.makeText(c, ""+mmsAddress, Toast.LENGTH_SHORT).show();
getAudioFromMMS(selectionPart,muri);
getVideoFromMMS(selectionPart,muri);

			}
		}
	}

	private void getVideoFromMMS(String selectionPart, Uri muri) {
		Cursor cPart = c.getContentResolver().query(muri, null, selectionPart,
				null, null);
		if (cPart.moveToFirst()) {
			do {
				String partId = cPart.getString(cPart.getColumnIndex("_id"));
				String type = cPart.getString(cPart.getColumnIndex("ct"));
				if ("video/mp4".equals(type) || "video/mpg".equals(type)
						|| "video/mpeg".equals(type) || "video/3gp".equals(type)
						|| "video/3gpp".equals(type)) {
					Uri mUri = getUriById(partId);
					Log.d("TAG_TO_TESTVideo Uri:",""+mUri);
				//	Toast.makeText(c, ""+mUri, Toast.LENGTH_SHORT).show();
				}
			} while (cPart.moveToNext());
		}
		
	}

	private void getAudioFromMMS(String selectionPart, Uri muri) {
		Cursor cPart = c.getContentResolver().query(muri, null, selectionPart,
				null, null);
		if (cPart.moveToFirst()) {
			do {
				String partId = cPart.getString(cPart.getColumnIndex("_id"));
				String type = cPart.getString(cPart.getColumnIndex("ct"));
				if ("audio/mp3".equals(type) || "audio/mid".equals(type)
						|| "audio/midi".equals(type) || "audio/wav".equals(type)
						 || "audio/emy".equals(type) || "audio/amr".equals(type)
						|| "audio/imy".equals(type)) {
					Uri mUri = getUriById(partId);
					Log.d("TAG_TO_TESTAudio Uri:",""+mUri);
					//Toast.makeText(c, ""+mUri, Toast.LENGTH_SHORT).show();
				}
			} while (cPart.moveToNext());
		}
		
	}

	private Uri getUriById(String partId) {
		Uri partURI = Uri.parse("content://mms/part/" + partId);
		return partURI;
	}

	private void getImageFromMMS(String selectionPart, Uri uri) {
		Cursor cPart = c.getContentResolver().query(uri, null, selectionPart,
				null, null);
		if (cPart.moveToFirst()) {
			do {
				String partId = cPart.getString(cPart.getColumnIndex("_id"));
				String type = cPart.getString(cPart.getColumnIndex("ct"));
				if ("image/jpeg".equals(type) || "image/bmp".equals(type)
						|| "image/gif".equals(type) || "image/jpg".equals(type)
						|| "image/png".equals(type)) {
					Bitmap bitmap = getMmsImage(partId);
				}
			} while (cPart.moveToNext());
		}

	}

	private Bitmap getMmsImage(String _id) {
		Uri partURI = Uri.parse("content://mms/part/" + _id);
		Log.d("TAG_TO_TESTImage Uri:",""+partURI);
		//Toast.makeText(c, ""+partURI, Toast.LENGTH_SHORT).show();
		InputStream is = null;
		Bitmap bitmap = null;
		try {
			is = c.getContentResolver().openInputStream(partURI);
			bitmap = BitmapFactory.decodeStream(is);
		} catch (IOException e) {
		} finally {
			if (is != null) {
				try {
					Log.d("TAG_TO_TESTimage bitmat", "Bitmat conversion successful");
					is.close();
				} catch (IOException e) {
				}
			}
		}
		return bitmap;
	}

	private void getTextFromMMS(String selectionPart, Uri muri) {
		Cursor cursor = c.getContentResolver().query(muri, null, selectionPart,
				null, null);
		if (cursor.moveToFirst()) {
			do {
				String partId = cursor.getString(cursor.getColumnIndex("_id"));
				String mtype = cursor.getString(cursor.getColumnIndex("ct"));
				if ("text/plain".equals(mtype)) {
					String data = cursor.getString(cursor
							.getColumnIndex("_data"));
					String body;
					if (data != null) {
						// implementation of this method below
						body = getMmsText(partId);
						Log.d("TAG_TO_TEST_Data", "" + body);
					} else {
						body = cursor.getString(cursor.getColumnIndex("text"));
						Log.d("TAG_TO_TESTTextData", "" + body);
					}
				}
			} while (cursor.moveToNext());
		}
	}

	private String getMmsText(String id) {
		Uri partURI = Uri.parse("content://mms/part/" + id);
		InputStream is = null;
		StringBuilder sb = new StringBuilder();
		try {
			is = c.getContentResolver().openInputStream(partURI);
			if (is != null) {
				InputStreamReader isr = new InputStreamReader(is, "UTF-8");
				BufferedReader reader = new BufferedReader(isr);
				String temp = reader.readLine();
				while (temp != null) {
					sb.append(temp);
					temp = reader.readLine();
				}
			}
		} catch (IOException e) {
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
				}
			}
		}
		return sb.toString();
	}
	public static String getMMSAddress(Context context, String id) {
	    String addrSelection = "type=151 AND msg_id=" + id;
	    String uriStr = MessageFormat.format("content://mms/{0}/addr", id);
	    Uri uriAddress = Uri.parse(uriStr);
	    String[] columns = { "address" };
	    Cursor cursor = context.getContentResolver().query(uriAddress, columns,
	            addrSelection, null, null);
	    String address = "";
	    String val;
	    if (cursor.moveToFirst()) {
	        do {
	            val = cursor.getString(cursor.getColumnIndex("address"));
	            if (val != null) {
	                address = val;
	                // Use the first one found if more than one
	                break;
	            }
	        } while (cursor.moveToNext());
	    }
	    if (cursor != null) {
	        cursor.close();
	    }
	    // return address.replaceAll("[^0-9]", "");
	    return address;
	}
	@Override
	public void onChange(boolean selfChange) {
		// TODO Auto-generated method stub
		onChange(selfChange, null);
	}

}
