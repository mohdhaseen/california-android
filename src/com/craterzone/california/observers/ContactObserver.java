package com.craterzone.california.observers;

import java.util.ArrayList;

import com.craterzone.california.PhoneBookManager;
import com.craterzone.california.models.PhoneBookContacts;

import android.content.Context;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by mohdhaseen on 6/5/15.
 */
public class ContactObserver extends ContentObserver {

	Context c;

	public ContactObserver(Handler handler, Context context) {
		super(handler);
		c = context;
	}
	
	@Override
	public void onChange(boolean selfChange, Uri uri) {
		if (uri != null) {
			Toast.makeText(c, "refreshing data", Toast.LENGTH_SHORT)
			.show();
			
	
			ArrayList<PhoneBookContacts> a1=PhoneBookManager.getPhoneBookContacts(c);
			PhoneBookContacts c1;
			for(int i=0;i<a1.size();i++)
			{
				c1=a1.get(i);
				Log.i("DATE", ""+c1.getContactId()+"-"+c1.getName());
				Log.i("DATE", ""+c1.getPhoneNumbers());
				Log.i("DATE", ""+c1.getEmails());
				Toast.makeText(c, ""+c1.getContactId()+"-"+c1.getName(), Toast.LENGTH_SHORT).show();
				Toast.makeText(c, ""+c1.getPhoneNumbers(), Toast.LENGTH_SHORT).show();
				Toast.makeText(c, ""+c1.getEmails(), Toast.LENGTH_SHORT).show();
			}
	
		} else {
			Toast.makeText(c, "null uri ", Toast.LENGTH_SHORT).show();
		}

	}

}
