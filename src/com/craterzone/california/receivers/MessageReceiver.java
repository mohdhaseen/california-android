package com.craterzone.california.receivers;

import Utilities.UtilityDateTime;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by mohdhaseen on 7/5/15.
 */
public class MessageReceiver extends BroadcastReceiver {
	private static final String TAGBODY = "body";
	private static final String TAGNUMBER = "number";
	private static final String TAGCURRENTDATE = "date";

	
	
	@Override
	public void onReceive(Context context, Intent intent) {

		if (intent.getAction()
				.equals("android.provider.Telephony.SMS_RECEIVED")) {

			Bundle pudsBundle = intent.getExtras();
			Object[] pdus = (Object[]) pudsBundle.get("pdus");
			SmsMessage sms = SmsMessage.createFromPdu((byte[]) pdus[0]);
			
			Log.d(TAGNUMBER, sms.getOriginatingAddress());
			Log.d(TAGBODY, sms.getMessageBody());
			String formattedDate = UtilityDateTime.getDateFromTimestamp(sms.getTimestampMillis());
			Log.d(TAGCURRENTDATE, formattedDate);

			Toast.makeText(context, "A new message Received ",
					Toast.LENGTH_SHORT).show();
			Toast.makeText(context, "Date of message is - "+formattedDate,
					Toast.LENGTH_SHORT).show();
			Toast.makeText(context, "Sender is - "+sms.getOriginatingAddress(),
					Toast.LENGTH_SHORT).show();
			Toast.makeText(context,"SMS Body is - -"+sms.getMessageBody(),
					Toast.LENGTH_SHORT).show();
		}
		
	
	}
}
