package com.craterzone.california.receivers;

import com.craterzone.california.RecordAudio;

import Utilities.Constants;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

public class CallReceiver extends BroadcastReceiver {
	private String phoneNumber;
	RecordAudio recordAudio;

	@Override
	public void onReceive(Context context, Intent intent) {

		try {

			phoneNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
			String extraState = intent
					.getStringExtra(TelephonyManager.EXTRA_STATE);
			if (extraState != null) {
				if (extraState.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
					Toast.makeText(context, "startRecording", Toast.LENGTH_SHORT)
							.show();
					Log.d(Constants.TAG, "RecordingStarted");
					recordAudio = RecordAudio.getInstance(context);
					if (!recordAudio.isRecording()) {
						recordAudio.startRecording();
					}
				} else if (extraState.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
					Toast.makeText(context, "stopRecording", Toast.LENGTH_SHORT)
							.show();

					Log.d(Constants.TAG, "RecordingStoped");
					recordAudio = RecordAudio.getInstance(context);
					if (recordAudio.isRecording()) {
						recordAudio.stopRecording();
					}
				} else if (extraState
						.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
					Log.d(Constants.TAG, "Ringing");
					if (phoneNumber == null) {
						phoneNumber = intent
								.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
						Toast.makeText(context,
								"inpnhoennumber=" + phoneNumber,
								Toast.LENGTH_SHORT).show();
					}
				}
			} else if (phoneNumber != null) {
				Toast.makeText(context, "outpnhoennumber=" + phoneNumber,
						Toast.LENGTH_SHORT).show();
			}
		} catch (Exception e) {
			Log.d("Exception", "" + e);
			e.printStackTrace();
		}

	}
}
