package com.craterzone.california;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.widget.Toast;

import com.craterzone.california.models.PhoneBookContacts;

public class PhoneBookManager {

	private static ArrayList<PhoneBookContacts> allContactList = null;
	private static HashMap<Long, PhoneBookContacts> allContactsMap = null;

	public static ArrayList<PhoneBookContacts> readAllContacts(Context context) {

		allContactList = new ArrayList<PhoneBookContacts>();
		allContactsMap = new HashMap<Long, PhoneBookContacts>();

		ContentResolver cr = context.getContentResolver();
		Cursor contactIdCur = cr.query(
				ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
				new String[] {
						ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
						ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,

				}, null, null, null);

		while (contactIdCur.moveToNext()) {

			Long contactId = contactIdCur
					.getLong(contactIdCur
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));

			String name = contactIdCur
					.getString(contactIdCur
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));

			// Create a contact object
			PhoneBookContacts contact = new PhoneBookContacts();
			contact.contactId = contactId;
			contact.name = name;
			contact.bitmap = loadContactPhoto(context.getContentResolver(),
					contact.contactId);

			// Read all phone numbers regarding this contact
			Cursor phoneCur = cr
					.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
							new String[] { ContactsContract.CommonDataKinds.Phone.NUMBER },
							ContactsContract.CommonDataKinds.Phone.CONTACT_ID
									+ " = ?",
							new String[] { contact.contactId.toString() }, null);

			while (phoneCur.moveToNext()) {
				// This would allow you get several phone numbers
				String phoneNumber = phoneCur
						.getString(phoneCur
								.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
				contact.addPhoneNumber(phoneNumber);
			}

			phoneCur.close();

			// Read all Emails regarding this contact
			Cursor emailCur = cr
					.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
							new String[] { ContactsContract.CommonDataKinds.Email.ADDRESS },
							ContactsContract.CommonDataKinds.Email.CONTACT_ID
									+ " = ?",
							new String[] { contact.contactId.toString() }, null);

			while (emailCur.moveToNext()) {
				// This would allow you get several emails
				String email = emailCur
						.getString(emailCur
								.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));

				contact.addEmail(email);
			}

			emailCur.close();
			
			allContactList.add(contact);
			allContactsMap.put(contact.contactId, contact);
		}

		contactIdCur.close();

		return allContactList;
	}

	public static Bitmap loadContactPhoto(ContentResolver cr, long id) {
		Uri uri = ContentUris.withAppendedId(
				ContactsContract.Contacts.CONTENT_URI, id);
		InputStream input = ContactsContract.Contacts
				.openContactPhotoInputStream(cr, uri);
		if (input == null) {
			return null;
		}
		return BitmapFactory.decodeStream(input);
	}

	public static ArrayList<PhoneBookContacts> getPhoneBookContacts(
			Context context) {
			readAllContacts(context);
		
		return allContactList;
	}

	public static PhoneBookContacts getContactByContactId(Long id) {
		if (allContactsMap != null) {
			return allContactsMap.get(id);
		}
		return null;
	}

}
