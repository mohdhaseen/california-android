package com.craterzone.california;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.craterzone.california.R;
import com.craterzone.california.services.ServiceForContacts;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		 Intent intent= new Intent(this, ServiceForContacts.class);
	        startService(intent);
	}

}
