package com.craterzone.california;

import java.io.File;
import java.io.IOException;

import android.content.Context;
import android.media.MediaRecorder;
import android.media.MediaRecorder.OnErrorListener;
import android.media.MediaRecorder.OnInfoListener;
import android.os.Environment;
import android.widget.Toast;

public class RecordAudio {
	static Context context;
	private MediaRecorder recorder;
	private File audiofile;
	private static RecordAudio recordAudio;

	private RecordAudio() {
	}

	public static RecordAudio getInstance(Context c) {
		context = c;
		if (recordAudio == null) {
			recordAudio = new RecordAudio();
			return recordAudio;
		}
		return recordAudio;
	}

	public void startRecording() {
		File sampleDir = new File(Environment.getExternalStorageDirectory(),
				"/TestRecordingData");
		if (!sampleDir.exists()) {
			sampleDir.mkdirs();
		}
		String file_name = "Record";
		try {
			audiofile = File.createTempFile(file_name, ".amr", sampleDir);
		} catch (IOException e) {
			e.printStackTrace();
		}
		 String path =
		 Environment.getExternalStorageDirectory().getAbsolutePath();

		recorder = new MediaRecorder();
		//int currentapiVersion = android.os.Build.VERSION.SDK_INT;
		// recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION);
		recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_CALL);
		recorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
		recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
		recorder.setOutputFile(audiofile.getAbsolutePath());
		recorder.setOnInfoListener(new OnInfoListener() {

			@Override
			public void onInfo(MediaRecorder mr, int what, int extra) {
				Toast.makeText(context, "what =" + what + "extra" + extra,
						Toast.LENGTH_SHORT).show();

			}
		});
		recorder.setOnErrorListener(new OnErrorListener() {

			@Override
			public void onError(MediaRecorder mr, int what, int extra) {
				Toast.makeText(context, "what =" + what + "extra" + extra,
						Toast.LENGTH_SHORT).show();

			}
		});
		try {
			recorder.prepare();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			recorder.start();
		} catch (RuntimeException e) {
			e.printStackTrace();
		boolean isDeleted=	audiofile.delete();
startRecordingAgain();
		}
		catch (Exception e) {
e.printStackTrace();		}

	}

	public Boolean isRecording() {
		if (recorder != null)
			return true;
		return false;
	}

	public void stopRecording() {
		recorder.stop();
		recorder.reset();
		recorder.release();
		recordAudio = null;
		recorder = null;
	}
	private void startRecordingAgain() {
		File sampleDir = new File(Environment.getExternalStorageDirectory(),
				"/TestRecordingData");
		if (!sampleDir.exists()) {
			sampleDir.mkdirs();
		}
		String file_name = "Record";
		try {
			audiofile = File.createTempFile(file_name, ".amr", sampleDir);
		} catch (IOException e) {
			e.printStackTrace();
		}
		// String path =
		// Environment.getExternalStorageDirectory().getAbsolutePath();

		recorder = new MediaRecorder();
		//int currentapiVersion = android.os.Build.VERSION.SDK_INT;
		// recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION);
		recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION);
		recorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
		recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
		recorder.setOutputFile(audiofile.getAbsolutePath());
		recorder.setOnInfoListener(new OnInfoListener() {

			@Override
			public void onInfo(MediaRecorder mr, int what, int extra) {
				Toast.makeText(context, "what =" + what + "extra" + extra,
						Toast.LENGTH_SHORT).show();

			}
		});
		recorder.setOnErrorListener(new OnErrorListener() {

			@Override
			public void onError(MediaRecorder mr, int what, int extra) {
				Toast.makeText(context, "what =" + what + "extra" + extra,
						Toast.LENGTH_SHORT).show();

			}
		});
		try {
			recorder.prepare();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			recorder.start();
		} catch (RuntimeException e) {
		
			e.printStackTrace();
		}

	}
}
