package databases;

public enum Tables {
	FBMESSAGES("fbmessages"),
	THREADS("threads");
	private String label;

	private Tables(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	
	public enum FBMessages {

		COL_MSGID("msgid"),COL_THREADKEY("threadkey"), COL_TEXT("text"),
		COL_SENDER("sender"), COL_ATTACHMENT("attachment"), COL_COORDINATES("coordinates") , COL_PENDING_SEND_MEDIA_ATTACHMENT_URI("pending_send_media_attachment_uri");
		private String label;

		private FBMessages(String label) {
			this.label = label;
		}

		public String getLabel() {
			return label;
		}

		public static String createTableQuery() {
			return "CREATE TABLE IF NOT EXISTS " + FBMESSAGES + "("
					+ COL_MSGID.getLabel() + " text, " + COL_THREADKEY.getLabel()
					+ " text , " + COL_TEXT.getLabel() + " text , " + COL_SENDER.getLabel() 
					+ " text , " + COL_ATTACHMENT.getLabel() + " text, " + COL_COORDINATES.getLabel()
					+ " text , " + COL_PENDING_SEND_MEDIA_ATTACHMENT_URI.getLabel() + " text" + ")";
		}

	}
	public enum Threads {

		COL_THREAD_KEY("thread_key"),COL_PARTICIPANTS("participants");
		private String label;

		private Threads(String label) {
			this.label = label;
		}

		public String getLabel() {
			return label;
		}

		public static String createTableQuery() {
			return "CREATE TABLE IF NOT EXISTS " + FBMESSAGES + "("
					+ COL_THREAD_KEY.getLabel() + " text, " + COL_PARTICIPANTS.getLabel()
					+ " text " + ")";
		}

	}
	

}
