package databases;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

public class FBSqliteHelper extends SQLiteOpenHelper{

	public static final String TAG = FBSqliteHelper.class.getName();

	public static final int DATABASE_VERSION = 1;
	/* can't change this name */
	public static final String DATABASE_NAME = Environment.getDataDirectory()+"";
	private static FBSqliteHelper _instance = null;
	private int openedConnections = 0;

	private FBSqliteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	/**
	 * Getting instance of Sqlite class
	 * 
	 * @param context
	 * @return instance Sqlite Helper
	 */
	public static FBSqliteHelper getInstance(Context context) {
		if (_instance == null) {
			synchronized (FBSqliteHelper.class) {
				if (_instance == null) {
					_instance = new FBSqliteHelper(context);
				}
			}
		}
		return _instance;
	}

	/**
	 * Count total no. instance of Readable DataBase created
	 */
	public synchronized SQLiteDatabase getReadableDatabase() {
		openedConnections++;
		return getReadableDatabase();
	}

	public synchronized void close() {
		openedConnections--;
		if (openedConnections == 0) {
			close();
		}
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		
			try {
				db.execSQL(Tables.FBMessages.createTableQuery());
				db.execSQL(Tables.Threads.createTableQuery());
				Log.i(TAG, " table created : ");
			} catch (SQLException e) {
				Log.e(TAG, " Unable to create table", e);
		} 
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + Tables.FBMESSAGES.getLabel());
		db.execSQL("DROP TABLE IF EXISTS " + Tables.THREADS.getLabel());
		onCreate(db);
	}

	public static void close(Cursor cursor) {
		try {
			if (cursor != null) {
				cursor.close();
			}
		} catch (Exception e) {
			Log.e(TAG, " Unable to close cursur", e);
		}
	}



}
