package databases;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.craterzone.california.models.FBModel;

public class DaoNew {
	public static final String TAG = FBDao.class.getName();
	private SQLiteDatabase database;
	private SqliteHelperNew dbHelper = null;
	private static DaoNew _instance;
	private Context _context;

	private DaoNew(Context context) {
		if (dbHelper == null) {
			this._context = context;

			dbHelper = SqliteHelperNew.getInstance(context);
		}
	}

	public static DaoNew getInstance(Context context) {
		if (_instance == null) {
			_instance = new DaoNew(context);
		}
		return _instance;
	}

	public boolean isOpen() {
		if (database != null) {
			return database.isOpen();
		}
		return false;
	}

	public void open() throws SQLException {
		if (dbHelper == null) {
			dbHelper = SqliteHelperNew.getInstance(_context);
		} else {
			try {
				database = dbHelper.getWritableDatabase();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void close() {
		if (dbHelper != null) {
			dbHelper.close();
		}
	}

	/**
	 * Open DB if Closed
	 */
	public void openDBIfClosed() {
		if (!isOpen()) {
			open();
		}
	}

	/**
	 * Insert user in DB if already exist then replace user
	 * 
	 * @param user
	 */
	public void insertMsgDetails(FBModel fbModel) {

		openDBIfClosed();
		try {
			if (database != null) {
				String sql = "INSERT INTO " + Tables.FBMESSAGES.getLabel()
						+ " VALUES (?,?,?,?,?,?,?);";
				SQLiteStatement statement = database.compileStatement(sql);
				database.beginTransaction();
				statement.bindString(1, fbModel.getMsgId());
				statement.bindString(2, fbModel.getThreadKey());
				statement.bindString(3, fbModel.getText());
				statement.bindString(4, fbModel.getSender());
				statement.bindString(5, fbModel.getAttachment());
				statement.bindString(6, fbModel.getCoordinates());
				statement.bindString(7, fbModel.getPendingMedia());
				statement.execute();
				database.setTransactionSuccessful();
				database.endTransaction();
			}
		} catch (Exception e) {
			Log.e(TAG, "Enable to Add Patient in Database :", e);
		}
	}

	
	/**
	 * Get member List From DB
	 * 
	 * @return member List and if error or not found then null
	 */
	public ArrayList<FBModel> getMsgDetails() {
		/**
		 * Check if DB is open : if not open then open
		 */
		openDBIfClosed();
		Cursor cursor = null;
		ArrayList<FBModel> msgDetails = new ArrayList<FBModel>();
		try {
			if (database != null) {
				//String query = "SELECT * FROM " + Tables.FBMESSAGES.getLabel();
				String query = "SELECT * FROM MESSAGES";
				cursor = database.rawQuery(query, null);
				if (cursor != null) {
					while (cursor.moveToNext()) {
						FBModel msgRow=new FBModel();
						msgRow.setMsgId(cursor.getString(0));
						msgRow.setThreadKey(cursor.getString(1));
						msgRow.setText(cursor.getString(2));
						msgRow.setSender(cursor.getString(3));
						msgRow.setAttachment(cursor.getString(4));
						msgRow.setCoordinates(cursor.getString(5));
						msgRow.setPendingMedia(cursor.getString(6));
	//					msgRow.setParticipants(cursor.getString(7));
						msgDetails.add(msgRow);
				  }
				}
			}
		} catch (Exception e) {
			Log.e(TAG, "Unable to get data from database", e);
		} finally {
			FBSqliteHelper.close(cursor);
		}
		return msgDetails;
	}

	public String getStringFromDB(String query) {
		openDBIfClosed();
		String result = null;
		try {
			if (database != null) {
				Cursor cursor = database.rawQuery(query, null);
				if (cursor != null && cursor.moveToFirst()) {
					result = cursor.getString(0);
				}
				if (cursor != null) {
					cursor.close();
				}
			}
		} catch (Exception e) {
			Log.e(TAG, "Unable to get data from database", e);
		}
		return result;
	}

	/**
	 * This method return long from db
	 * 
	 * @param query
	 * @return : return long from db or 0 if some error
	 */
	public long getLongFromDB(String query) {
		openDBIfClosed();
		Cursor cursor = null;
		long result = 0;
		try {
			if (database != null) {
				cursor = database.rawQuery(query, null);
				if (cursor != null && cursor.moveToFirst()) {
					result = cursor.getLong(0);
				}
			}
		} catch (Exception e) {
			Log.e(TAG, "Unable to get data from database", e);
		} finally {
			FBSqliteHelper.close(cursor);
		}
		return result;
	}

}
